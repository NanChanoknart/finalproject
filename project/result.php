
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Assembled 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20121231

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chanoknart</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href="defaulte.css" rel="stylesheet" type="text/css" media="all" />


  
<!--[if IE 6]>
<link href="default_ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="logo">
				<h1>Technology News Search</h1>
				
			</div>
		</div>
	<!-- end #header -->
	<div id="menu">
		<ul>
			<li><a href="searchhome.html">Home</a></li>
			<li><a href="allnewspage.php">All News</a></li>
			<li><a href="tag.html">Tags</a></li>
		
		</ul>
	</div>
  <!--end menu-->
  
	 <div id="content">
		<section>			
			<div class="post">
			<?php
	
  			$quey = trim(strtolower($_GET["quey"]));
  			//$quey = "googles";
			$handle = opendir("output_news19-6-59/output_word");
			$arraySim = array();
	
  			$count = 0;
    		while ($entry = readdir($handle)) {
    			$score = 0;
    			if(($entry==".")||($entry=="..")){ continue;  }
        			$text = file("output_news19-6-59/output_word/".$entry);
          				foreach($text as $value){
          					$text = trim(strtolower($value));

          		//print $text;
          					similar_text($quey,$text, $percent);
          					$similar = (int)$percent;
          		//print $similar;
          					if ($similar == 100) {
          		 				$score = $score+$similar;
          		 	//echo $score;
          		 			}
          				}
            	if ($score > 0) {
              		$arraySim[$entry] = $score;
              		$count++;
           		 }
           	
    		}
    		closedir($handle);
    		$handles = opendir("output_news19-6-59/output_word");
    		if ($count == 0){
      			while ($entry = readdir($handles)) {
      				 $score = 0;
        			if(($entry==".")||($entry=="..")){ continue;  }
          				$text = file("output_news19-6-59/output_word/".$entry);
          				foreach($text as $value){
            				$text = trim(strtolower($value));

            //print $text;
            				similar_text($quey,$text, $percent);
            				$similar = (int)$percent;
            //print $similar;
            				if ($similar >= 90) {
                				$score = $score+$similar;
                //echo $score;
            				}
          				}
          			if ($score > 0) {
            			$arraySim[$entry] = $score;
              
          			}
            
      			}
    		}
    closedir($handles); 
    arsort($arraySim);
    ?>


    		<?php 
    			$handle = opendir("output_news19-6-59/output_json");
    			foreach($arraySim as $x=>$x_value):
    				
    		?>
				

				<?php
					$name = substr($x,0,-3);
					$jsondata = file_get_contents("output_news19-6-59/output_json/".$name."json");
					$array = json_decode($jsondata,true);

  				?>
  				<h2><?php echo $array['News']; ?></h2>


				<p><a href=<?php echo $array['Image-Link'][0]; ?>><img src=<?php echo $array['Image-Link'][0]; ?> alt="" class="img-alignleft"></a><?php echo "By :".$array['By']." Date :".$array['Date']." Time :".$array['Time']; ?></p>
				<p><?php echo $array['Contents']; ?></p>
				
				<form action="onenews.php" method="post">
				<input type="hidden" name="filejson" value=<?php echo $name."json"; ?> />
				<input id="button-style" type="submit" value="Read More" />
				
				
   				
				<?php 
				
				endforeach; ?>
			</div>

		</section>
		
	</div>
<!--end content -->
	
	
</div>
</body>
</html>
