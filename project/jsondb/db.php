<?php
    // open mysql connection
    $host = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "data";
    $con = mysqli_connect($host, $username, $password, $dbname) or die('Error in Connecting: ' . mysqli_error($con));

    // use prepare statement for insert query
    $st = mysqli_prepare($con, 'INSERT INTO news(By, Contents, News, Tags) VALUES (?, ?, ?, ?)');

    // bind variables to insert query params
    mysqli_stmt_bind_param($st, 'ssss', $By, $Contents, $News, $Tags);

 /*   // read json file
    $filename = 'data.json';
    $json = file_get_contents($filename);   

    //convert json object to php associative array
    $data = json_decode($json, true);

    // loop through the array
    foreach ($data as $row) {
        // get the employee details
        $By = $row['By'];
        $Contents = $row['Contents'];
        
        $News = $row['News'];
        $Tags = $row['Tags'];
        
        
        // execute insert query
        mysqli_stmt_execute($st);
    }
    
    //close connection
    mysqli_close($con);

    */
?>