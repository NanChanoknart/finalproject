
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Assembled 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20121231

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chanoknart</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href="defaulte.css" rel="stylesheet" type="text/css" media="all" />


  
<!--[if IE 6]>
<link href="default_ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="logo">
				<h1>Technology News Search</h1>
				<p>Template Design by <a href="http://www.freecsstemplates.org">FCT</a></p>
			</div>
		</div>
	<!-- end #header -->
	<div id="menu">
		<ul>
			<li><a href="searchhome.html">Home</a></li>
			<li><a href="allnewspage.php">All News</a></li>
			<li><a href="tag.html">Tags</a></li>
		
		</ul>
	</div>
  <!--end menu-->
  
	 <div id="content">
	 	<?php
	 		$s1 = $_POST["filejson"];
	 		$jsondata = file_get_contents("output_news19-6-59/output_json/".$s1);
			$array = json_decode($jsondata,true);

	 		
	 	?>
		
		<section>			
			<div class="post">
				<h2><?php echo $array['News']; ?></h2>


				<p><a href=<?php echo $array['Image-Link'][0]; ?>><img src=<?php echo $array['Image-Link'][0]; ?> alt="" class="img-alignleft"></a><?php echo "By :".$array['By']." Date :".$array['Date']." Time :".$array['Time']; ?></p>
				<p><?php echo $array['Contents']; ?></p>

				
				<span>From:</span>
				<span><a href=<?php echo $array['Link']; ?>><?php echo $array['Link']; ?></a></span>
				<p> </p>
				<span>Tags:</span>
				<?php foreach($array['Tags'] as $value): ?>

				<span><?php echo $value; ?></span>
				<?php endforeach; ?>

				
			</div>
		</section>
	</div>
<!--end content -->
	
	
</div>
</body>
</html>
