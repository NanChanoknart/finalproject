<?php 
date_default_timezone_set('Asia/Bangkok');
// -- database connection variable --
$db_hostname = "127.0.0.1";
$db_username = "root";
$db_password = "";
$db_name = "db";

// database connection
$objConnect = @mysqli_connect($db_hostname,$db_username,$db_password) or die ("ติดต่อฐานข้อมูลไม่ได้");
$objDB = mysqli_select_db($objConnect,$db_name) or die ("เลือกฐานข้อมูลไม่ได้".mysqli_error());

// setting thai language in mysql
mysqli_query($objConnect,"SET character_set_results=utf8");
mysqli_query($objConnect,"SET character_set_client=utf8");
mysqli_query($objConnect,"SET character_set_connection=utf8");
//mysqli_query($objConnect,"set collation_connection='utf-8_unicode_ci'");

$ITEM_PER_PAGE = "100";

$PROJECT_LIST_PER_PAGE = "10";

$SHOW_DATE = date("Y/m/t H:i:s");

?>