
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Assembled 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20121231

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chanoknart</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href="defaulte.css" rel="stylesheet" type="text/css" media="all" />


  
<!--[if IE 6]>
<link href="default_ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="logo">
				<h1>Technology News Search</h1>
				
			</div>
		</div>
	<!-- end #header -->
	<div id="menu">
		<ul>
			<li><a href="searchhome.html">Home</a></li>
			<li class="current_page_item" ><a href="#">All News</a></li>
			<li><a href="tag.html">Tags</a></li>
		
		</ul>
	</div>
  <!--end menu-->
  
	 <div id="content">
		<section>			
			<div class="post">

			<?php 
    			$handle = opendir("output_news19-6-59/output_json");
    			$i = 0;
				while ($entry = readdir($handle)):?>
			
					<?php if(($entry==".")||($entry=="..")) : ?>
						<?php continue; ?>
					<?php endif; ?>

				<?php
					$jsondata = file_get_contents("output_news19-6-59/output_json/".$entry);
					$array = json_decode($jsondata,true);
  				?>
  				<h2><?php echo $array['News']; ?></h2>


				<p><a href=<?php echo $array['Image-Link'][0]; ?>><img src=<?php echo $array['Image-Link'][0]; ?> alt="" class="img-alignleft"></a><?php echo "By :".$array['By']." Date :".$array['Date']." Time :".$array['Time']; ?></p>
				<p><?php echo $array['Contents']; ?></p>
				
				<form action="onenews.php" method="post">
				<input type="hidden" name="filejson" value=<?php echo $entry; ?> />
				<input id="button-style" type="submit" value="Read More" />

   		<?php
   		endwhile;
		?>
			
			</div>
		</section>
		<section>			
			<div class="post">
				<h2>Title</h2>
				<p><a href="#"><img src="images/pics02.jpg" alt="" class="img-alignleft"></a>Consectetuer adipiscing elit. Nam pede erat, porta eu, lobortis eget, tempus et, tellus. Etiam neque. Vivamus consequat lorem at nisl. Nullam non wisi a sem semper eleifend. Donec mattis libero eget urna. Duis pretium velit ac mauris. Proin eu wisi suscipit nulla suscipit interdum. Aenean lectus lorem, imperdiet at, ultrices eget, ornare et, wisi. Pellentesque adipiscing purus ac magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.	 Pellentesque pede. Donec pulvinar ullamcorper metus. In eu odio at lectus pulvinar mollis. Consectetuer adipiscing elit. Nam pede erat, porta eu, lobortis eget, tempus et, tellus. Etiam neque. Vivamus consequat lorem at nisl. Nullam non wisi a sem semper eleifend. Donec mattis libero eget urna. Duis pretium velit ac mauris. Proin eu wisi suscipit nulla suscipit interdum. Aenean lectus lorem, imperdiet at, ultrices eget, ornare et, wisi. Pellentesque adipiscing purus ac magna. Pellentesque habitant morbi tristique.</p>
				<form action="onenews.php" method="get">
				<input id="button-style" type="submit" value="Read More" />
			</div>
		</section>
	</div>
<!--end content -->
	
	
</div>
</body>
</html>
